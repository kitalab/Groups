<?php
/**
 * Groups Viewのテストケース
 *
 * @author Yuna Miyashita <butackle@gmail.com>
 * @link http://www.netcommons.org NetCommons Project
 * @license http://www.netcommons.org/license.txt NetCommons License
 * @copyright Copyright 2014, NetCommons Project
 */

App::uses('GroupsTestBase', 'Groups.Test/Case');


/**
 * Groups Viewのテストケース
 *
 * @author Yuna Miyashita <butackle@gmail.com>
 * @package NetCommons\Groups\Test\Case\
 */
class GroupsViewTestBase extends GroupsTestBase {

/**
 * phpmdエラー回避用メソッド
 * 
 * @return void
 */
	public function testDummy() {
	}
}
